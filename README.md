<h1>Authentication and Authorization</h1>

<p>
   When talking about security two terminology always comes in picture
   <ul>
      <li>Authentication: the process of identifying of the user accessing the system(WHO YOU ARE?)</li>
      <li>Authorization: the process of identifying that what the user can do in the system(WHAT YOU CAN DO)</li>
   </ul>
   
   To make a process secure both authentication and authorization plays an important role. For authentication, the <username,password> pair is used most of the time. For authorization, roles are 
   created. The role identifies the permission granted to the user. As for example, in any system there might be two users
   <ul>
       <li>ADMIN: It has the maximum permission in the system. Possibly the role of the user is ROLE_ADMIN</li>
       <li>General User: It may have less permission that ADMIN. The role user may be ROLE_USER</li>
   </ul>
   Now if any resource in the system is allowed to be accessed only by user having role ROLE_ADMIN, then it must not be accessible by user in role ROLE_USER.
</p>

<h1>Steps of authentication and authorization in web based application</h1>
<p>
In normal case, the following steps are required to authorize and to authenticate

<ol>
   <li>A login screen, which consists of username and password, is provided to the user who is trying to access the protected resource</li>
   <li>The username and password is sent to the server and server try to authenticate the user</li>
   <li>After successfully authenticated the server retrieve its provided roles</li>
   <li>The roles are kept in the header under the named Authorization</li>
   <li>The user is provided a new page according to the role.</li>
   <li>In any subsequent request, the user must send the Authorization header so that the server can determin its role</li>
</ol>
<br>
Now a days many of the software provides "SINGLE SIGN ON" functionality where we can use our social media like facebook, linked in, google etc to perform sign in functionality. In this case 
security is much more important as some other device or software is trying to gain the access to sensitive data. In this scenario the usual flow will not work as more than one entity comes in 
picture.
</p>